# nftnl-rs (A Nftables manipulation library) ![600 logo](https://gitlab.com/4neko/nftnl-rs/-/raw/master/logo_600.png?ref_type=heads&inline=true)
# A repo is located at https://repo.4neko.org/4NEKO/nftnl-rs

This is a crate (currently in development) which implements a `netlink` protocol to communicate with
the Linux Nftables firewall.

## This crate is in its early development state. It is not planned to extend its functionality! Use at your own risk.

This crate was developed only for the tables/sets manipulations i.e add/del/get!!!

At the moment this crate allows to:
- perform operations on the sets i.e add IP, remove IP, get IP from the list.

For examples, see /examples/ directory.

Get IP from the table's set. i.e performing the following command:
```bash
$ sudo nft list set ip table-test table-set
```

```rust
use nftnl_rs::{netlink::{netlink::{NetlinkCb, NetlinkResponseReader, Nlmsghdr, MnlNlmsgBatch, NlmFFlags, NetlinkResponse, NetlinkRespRes, NetlinkCbArr, Nlmsgerr}, MNL_SOCKET_BUFFER_SIZE, linux::Nfproto, socket::mnl_socket}, boxed::Boxed, error::NtflRes, set::{nftnl_set, NftnlSetFlags}, nf_tables::NfTablesMsgTypes};
use rand::Rng;

pub struct NetlinkCbImplGet;
impl NetlinkCb for NetlinkCbImplGet
{
    fn mnl_cb(reader: &mut NetlinkResponseReader, _nlh: Boxed<Nlmsghdr>, _data: Option<u32>) -> Option<NtflRes<NetlinkRespRes>>
    {
        

        let mut t = nftnl_set::new();

        let res = t.nftnl_set_elems_nlmsg_parse(reader);

        if res.is_err() == true
        {
            return Some(res);
        }

        let res = res.unwrap();

        println!("{}", t);  
    
        return Some(Ok(res));
    }
}

pub struct NetlinkCbArrImpl;

impl NetlinkCbArr for NetlinkCbArrImpl
{
    fn mnl_cb_noop(reader: &mut NetlinkResponseReader, nlh: Boxed<Nlmsghdr>, data: Option<u32>) -> Option<NtflRes<NetlinkRespRes>>
    {
        return None;
    }

    fn mnl_cb_error(reader: &mut NetlinkResponseReader, nlh: Boxed<Nlmsghdr>, err: Boxed<Nlmsgerr>, data: Option<u32>) -> Option<NtflRes<NetlinkRespRes>>
    {
        return None;
    }

    fn mnl_cb_stop(reader: &mut NetlinkResponseReader, nlh: Boxed<Nlmsghdr>, data: Option<u32>) -> Option<NtflRes<NetlinkRespRes>>
    {
        return None;
    }
}


fn main()
{
    let mut rng = rand::thread_rng();
    let mut seq: u32 = rng.gen();

    let mut nl_set = nftnl_set::new();

    nl_set.nftnl_set_set_str(NftnlSetFlags::NFTNL_SET_TABLE, "table-test").unwrap();
	nl_set.nftnl_set_set_str(NftnlSetFlags::NFTNL_SET_NAME, "table-set").unwrap();
    nl_set.nftnl_set_set_u32(NftnlSetFlags::NFTNL_SET_ID, 1).unwrap();

    let mut batch = MnlNlmsgBatch::mnl_nlmsg_batch_start(*MNL_SOCKET_BUFFER_SIZE);

    // root
    let mut nlh = 
        Nlmsghdr::nftnl_nlmsg_build_hdr(
            &mut batch, 
            NfTablesMsgTypes::NFT_MSG_GETSETELEM, 
            Nfproto::NFPROTO_IPV4, 
            NlmFFlags::NLM_F_DUMP | NlmFFlags::NLM_F_ACK,
            seq
        )
        .unwrap();

    seq += 1;

    nl_set.nftnl_set_elems_nlmsg_build_payload(&mut batch, &mut nlh).unwrap();

    drop(nl_set);

    println!("{}", batch);

    let mut nl = mnl_socket::mnl_socket_open(libc::NETLINK_NETFILTER).unwrap();

    nl.mnl_socket_bind(0, unsafe{ libc::getpid() }).unwrap();

    let portid = nl.mnl_socket_get_portid();

    let len = batch.mnl_nlmsg_batch_size();
    nl.mnl_socket_sendto(batch.mnl_nlmsg_batch_msg(), len).unwrap();

    
    let mut ret = nl.mnl_socket_recvfrom(*MNL_SOCKET_BUFFER_SIZE).unwrap();
 

    while ret.len() > 0
    {
        let resp = NetlinkResponse::<NetlinkCbImplGet, NetlinkCbArrImpl>::new(ret);
        
		let lret = resp.mnl_cb_run(0, portid, None).unwrap();
		if lret == NetlinkRespRes::MNL_CB_STOP
        {
			break;
        }

		ret = nl.mnl_socket_recvfrom(*MNL_SOCKET_BUFFER_SIZE).unwrap();
	}
}

```
